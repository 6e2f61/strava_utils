import csv
import gzip
import os
import shutil

from dateutil import parser


def parse_activities_csv():
    with open ("activities.csv", newline="") as csv_file:
        activities_reader = list(csv.DictReader(csv_file, delimiter=',', quotechar='"'))
        create_activity_type_dirs(activities_reader)
        filter_activities_with_files(activities_reader)
    return 0


def create_activity_type_dirs(activities_reader):
    print("Creating dirs...")
    activity_types = find_all_activity_types(activities_reader)
    for activity_type in activity_types:
        try:
            os.mkdir(activity_type)
        except FileExistsError:
            print(f"Folder {activity_type} already exists. Skipping...")
            continue
    return 0


def extract_activity_year(activity_date):
    return parser.parse(activity_date).year


def create_year_directory(activity):
    activity_year = extract_activity_year(activity["Activity Date"])
    try:
        os.mkdir(f"{activity['Activity Type']}/{activity_year}")
    except FileExistsError:
        pass
    return 0


def find_all_activity_types(activities_reader):
    return set((activity["Activity Type"] for activity in activities_reader))


def filter_activities_with_files(activities):
    print("Filtering files...")
    for activity in activities:
        if activity_file_exists(activity["Filename"]):
            if activity_is_gzipped(activity["Filename"]):
                activity["Filename"] = extract_gzipped_file(activity["Filename"])
            if activity_is_tcx(activity["Filename"]):
                fix_tcx_file(activity["Filename"])
            create_year_directory(activity)
            move_activity_file(activity)
    return 0


def move_activity_file(activity):
    """
    Files are overwritten in target path!
    """
    activity_year = extract_activity_year(activity["Activity Date"])
    try:
        shutil.move(
            activity["Filename"], 
            f"{activity['Activity Type']}/{activity_year}/{activity['Filename'].lstrip('activities')}"
            )
        print(
            f"ID: {activity['Activity ID'].ljust(10)}: File {activity['Filename'].ljust(26)}"
            f" moved to {activity['Activity Type']}/{activity_year}"
            )
    except FileNotFoundError:
        print(f"File {activity['Filename']} not found. Skipping...")
    return 0


def activity_file_exists(activity_filename):
    if os.path.exists(activity_filename):
        return True
    else:
        return False


def activity_is_gzipped(activity_filename):
    if ".gz" in activity_filename:
        return True
    else:
        return False


def activity_is_tcx(activity_filename):
    if ".tcx" in activity_filename:
        return True
    else:
         return False   


def remove_zipped_file(activity_file):
    os.remove(activity_file)
    print(f"File {activity_file} removed...")


def extract_gzipped_file(activity_filename):
    with gzip.open(activity_filename, "rb") as zipped_activity:
        with open(activity_filename.strip(".gz"), "wb") as activity:
            shutil.copyfileobj(zipped_activity, activity)
    remove_zipped_file(activity_filename)
    return activity_filename.rstrip(".gz")


def fix_tcx_file(activity_filename):
    with open(activity_filename, "rt") as tcx_file:
        original_file = tcx_file.readlines()
    with open(activity_filename, "wt") as new_tcx_file:
        original_file[0] = '<?xml version="1.0" encoding="UTF-8"?>\n'
        new_tcx_file.writelines(original_file)


if __name__ == "__main__":
    parse_activities_csv()
