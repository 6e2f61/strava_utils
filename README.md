Utility to sort activity files (\*.gpx \*.fit \*.tcx) by activity type (e.g. Running, Cycling, Swim) downloaded from strava profile.
Activities are 
- unzipped (if gzipped)
- tcx files headers are fixed (remove spaces)
- moved to folder with pattern `activity_folder/activity_year/files*`
- all gzipped files are removed
- by default all files are overwriten in destination directory without prompting

Requires python>=3

Script must be placed it folder along with file *activities.csv* and folder *activities*.
